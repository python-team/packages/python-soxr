Source: python-soxr
Section: python
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Josenilson Ferreira da Silva <nilsonfsilva@hotmail.com>
Rules-Requires-Root: no
Build-Depends: cmake,
               cython3,
               debhelper-compat (= 13),
               dh-sequence-numpy3,
               dh-sequence-python3,
               libsoxr-dev,
               nanobind-dev,
               pybuild-plugin-pyproject,
               python3-all,
               python3-all-dev,
               python3-numpy,
               python3-pytest <!nocheck>,
               python3-scikit-build-core,
               python3-setuptools,
               python3-setuptools-scm,
               python3-typing-extensions,
               python3-wheel,
# Build-Depends-Indep: dh-sequence-sphinxdoc,
#                      python3-sphinx <!nodoc>,
#                      python3-importlib-metadata <!nodoc>,
#                      python3-linkify-it <!nodoc>,
#                      python3-myst-parser <!nodoc>,
#                      python3-sphinx-book-theme <!nodoc>,
Standards-Version: 4.7.0
Homepage: https://github.com/dofuuz/python-soxr
Vcs-Browser: https://salsa.debian.org/python-team/packages/python-soxr
Vcs-Git: https://salsa.debian.org/python-team/packages/python-soxr.git
Testsuite: autopkgtest-pkg-pybuild

Package: python3-soxr
Architecture: any
Depends: ${misc:Depends},
         ${python3:Depends},
         ${shlibs:Depends}
Suggests: python-soxr-doc
Description: Convert one-dimensional sampling rate
 Soxr is a Python library designed to perform resampling of audio files,
 allowing you to convert between different sample rates efficiently and
 accurately. With this tool it is possible to obtain fast and very high
 quality results for any resampling ratio.
 .
 This package installs the library for Python 3.

Package: python-soxr-doc
Section: doc
Architecture: all
Depends: ${misc:Depends},
         ${python3:numpy},
         ${sphinxdoc:Depends}
Description: Convert one-dimensional sampling rate (cammon documentation)
 Soxr is a Python library designed to perform resampling of audio files,
 allowing you to convert between different sample rates efficiently and
 accurately. With this tool it is possible to obtain fast and very high
 quality results for any resampling ratio.
 .
 This is the common documentation package.
